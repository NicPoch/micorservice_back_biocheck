const MongoClient = require("mongodb").MongoClient;
const { ObjectId } = require("bson"); 
var assert = require("assert");

const dbName = "Biocheck";
const url = 'mongodb+srv://biocheckAdmin:biocheckAdmin@cluster0.zt3om.mongodb.net/'+dbName+'?retryWrites=true&w=majority';//"mongodb://localhost:27017";
let db=null;
const client = new MongoClient(url, { useUnifiedTopology: true });


const connect=async()=>{
    db=await(await client.connect()).db(dbName);   
    console.log("Connection to templates via:"+db.databaseName);
}
const getTemplates=async(callback)=>{
    db.collection("templates").find({}).toArray(ans=>{
        callback(202,ans);
    }).catch(err=>callback(500,"Error"))
}
const getTemplate=async(id,callback)=>{
    db.collection("templates").findOne({_id:new ObjectId(id)},(err,res)=>{
        assert.equal(err, null);
        if (res === null) {
          callback(404, "Such element doesn´t exist");
        } else {
          callback(200, res);
        }
    });
};

exports.connect=connect;
exports.getTemplate=getTemplate;
exports.getTemplates=getTemplates;